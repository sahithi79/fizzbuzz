package com.tw.FizzBuzz;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FizzBuzzTest {
    FizzBuzz fizzBuzz = new FizzBuzz();
    @Test
    void shouldReturn1WhenInputIsOne(){
        assertThat(fizzBuzz.of(1),is("1"));
    }

    @Test
    void shouldReturn2WhenInputIsTwo(){
        assertThat(fizzBuzz.of(2),is("2"));
    }

    @Test
    void shouldReturnFizzWhenInputIsMultipleOfThree(){
        assertThat(fizzBuzz.of(6),is("Fizz"));
    }
    @Test
    void shouldReturnFizzWhenInputIsMultipleOfFive(){
        assertThat(fizzBuzz.of(10),is("Buzz"));
    }

    @Test
    void shouldThrowExceptionWhenInputIsNotWithinTheRange1To100(){
        assertThrows(IllegalArgumentException.class, ()->{
            fizzBuzz.of(103);
        });
    }
}
